package com.example.management.api.transfare;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ViewInvoiceDto {
    private double price;
    private String createdDate;
    private String updateDateDate;
}
