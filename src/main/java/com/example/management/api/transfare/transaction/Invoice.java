package com.example.management.api.transfare.transaction;

import com.example.management.api.transfare.Customer;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

/**
 * The type Invoice.
 */
@Getter
@Setter
@Entity
@Table(name = "invoice")
@Accessors(chain = true)
public class Invoice {
    /*
     * Invoice ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    /**
     * Amount
     */
    @Column(name = "amount", nullable = false)
    private double amount;

    /**
     * Invoice Created Date
     */
    @Column(name = "created_date", nullable = false)
    private Date createdDate;

    /**
     * Invoice Updated Date
     */
    @Column(name = "updated_date", nullable = false)
    private Date updatedDate;

    /**
     * Relation with Customer
     */
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
