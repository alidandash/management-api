package com.example.management.api.transfare.transaction;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Invoice repository.
 */
@Repository
public interface InvoiceRepository extends CrudRepository<Invoice, Long> {
    /**
     * Find by customer id list.
     *
     * @param customerId the customer id
     * @return the list
     */
    List<Invoice> findByCustomer_Id(long customerId);


    /**
     * Delete by customer.
     *
     * @param customer_id the customer
     */
    void deleteByCustomer_Id(Long customer_id);
}
