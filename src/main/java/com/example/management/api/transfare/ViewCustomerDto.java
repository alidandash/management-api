package com.example.management.api.transfare;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ViewCustomerDto {
    private String firstName;
    private String lastName;
    private String createdDate;
    private String updateDateDate;
    private List<ViewInvoiceDto> invoices = new ArrayList<>();
}
