package com.example.management.api.transfare;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class AddCustomerDto {
    private String firstName;
    private String lastName;
    private List<Double> itemsPrice = new ArrayList<>();
}
