# Le CNAM – Génie Logiciel

## NOTE : Pour voir les details, ouvrir `README.pdf`

## Présentation Spring Boot JPA

GLG203-Architectures Logicielles Java(1)
Ali Dandach -1020 baal


## Contents

- L’objectif :
- Qu'est-ce que Spring Framework :
   - Les modules du framework Spring
- Qu'est-ce que Spring Boot :
- L’architecture ORM
- Java JPA
- Spring boot JPA


## L’objectif :

Prou comprendre Spring JPA, je dois définir :

1. Le but de Spring Framework et les principaux modules.
2. Le but de Spring boot.
3. L’architecture ORM.
4. Java JPA
5. Spring Boot JPA
6. Faire un exemple pour comprendre l’importance du Spring JPA


## Qu'est-ce que Spring Framework :

Spring est une framework open-source développé par pivotal team à un but de réduire la
complexité des applications d'entreprise et de rendre productif le développement
d'applications, particulièrement les applications d'entreprises. Spring est l'un des frameworks
Java EE les plus utilisés. Les concepts de base du framework Spring sont «l'injection de
dépendance» et la «programmation orientée aspect».

Le framework Spring peut également être utilisé dans les applications Java normales pour
réaliser un couplage lâche entre les différents composants en implémentant l'injection de
dépendances et nous pouvons effectuer des tâches transversales telles que la journalisation et
l'authentification en utilisant le support Spring pour la programmation orientée aspect.

La popularité a grandie au profit de la complexité de Java EE notamment pour ses versions
antérieures à la version 5 mais aussi grâce à la qualité et la richesse des fonctionnalités qu'il
propose :

1. un conteneur léger implémentant le design pattern IoC pour la gestion des objets et de
    leurs dépendances en offrant des fonctionnalités avancées concernant la configuration
    et l'injection automatique. Un de ses points forts est d'être non intrusif dans le code de
    l'application tout en permettant l'assemblage d'objets faiblement couplés.
2. une gestion des transactions par déclaration offrant une abstraction du gestionnaire de
    transactions sous-jacent.
3. faciliter le développement des DAO de la couche de persistance en utilisant JDBC, JPA,
    JDO ou une solution open source comme Hibernate, MyBatis.
4. un support pour un usage interne à Spring (notamment dans les transactions) ou
    personnalisé de l'AOP qui peut être mis en œuvre avec Spring AOP pour les objets gérés
    par le conteneur et/ou avec AspectJ.
5. faciliter la testabilité de l'application.
6. favorise l'intégration avec de nombreux autres frameworks notamment ceux de type
    ORM ou web.

### Les modules du framework Spring

Le framework Spring fournit environ 20 modules qui peuvent être utilisés sur la base d’une
application. les principaux modules :

1. **Spring Core :** le noyau, qui contient à la fois un ensemble de classes utilisées par toutes
    les briques du framework et le conteneur léger.
2. **Spring AOP :** le module de programmation orientée aspect, qui s’intègre fortement avec
    AspectJ, un framework de POA à part entière.
3. **Spring DAO :** qui constitue le socle de l’accès aux dépôts de données, avec notamment
    une implémentation pour JDBC. D’autres modules fournissent des abstractions pour
    l’accès aux données (solutions de mapping objet-relationnel, LDAP) qui suivent les


```
mêmes principes que le support JDBC. La solution de gestion des transactions de Spring
fait aussi partie de ce module.
```
4. **Spring ORM :** qui propose une intégration avec des outils populaires de mapping objet-
    relationnel, tels que Hibernate, JPA, EclipseLink ou iBatis.
5. **Spring WEB :** le module comprenant le support de Spring pour les applications Web. Il
    contient notamment Spring Web MVC, la solution de Spring pour les applications Web,
    et propose une intégration avec de nombreux frameworks Web et des technologies de
    vue.
6. **Spring Context :** Ce module supporte l’internationalisation (I18N), EJB, JMS, Basic
    Remoting.


## Qu'est-ce que Spring Boot :

Spring boot est un projet a un but de faciliter la configuration d’un projet Spring et de réduire le
temps alloué au démarrage d’un projet.

On a un site web https://start.spring.io/ qui générer rapidement la structure de votre projet en
y incluant toutes les dépendances Maven nécessaires à votre application.

L’utilisation de « Starters » pour gérer les dépendances. Spring a regroupé les dépendances
Maven de Spring dans des « méga dépendances » afin de faciliter la gestion de celles-ci. Par
exemple si vous voulez ajouter toutes les dépendances pour gérer la « web deployment » il
suffit d’ajouter le starter « spring-boot-starter-web ».

L’auto-configuration, qui applique une configuration par défaut au démarrage de votre
application pour toutes dépendances présentes dans celle-ci. Cette configuration s’active à
partir du moment où vous avez annoté votre application avec « @EnableAutoConfiguration » ou
« @SpringBootApplication ». Bien entendu cette configuration peut-être surchargée via des
propriétés Spring prédéfinie ou via une configuration Java. L’auto-configuration simplifie la
configuration sans pour autant vous restreindre dans les fonctionnalités de Spring. Par exemple,
si vous utilisez le starter « spring-boot-starter-security », Spring Boot vous configurera la
sécurité dans votre application avec notamment un utilisateur par défaut et un mot de passe
généré aléatoirement au démarrage de votre application.

En plus de ces premiers éléments qui facilitent la configuration d’un projet, Spring Boot offre
d’autres avantages notamment en termes de déploiement applicatif. Habituellement, le
déploiement d’une application Spring nécessite la génération d’un fichier .war qui doit être
déployé sur un serveur comme un Apache Tomcat. Spring Boot simplifie ce mécanisme en
offrant la possibilité d’intégrer directement un serveur Tomcat dans votre exécutable. Au
lancement de celui-ci, un Tomcat embarqué sera démarré afin de faire tourner votre
application.

Enfin, Spring Boot met à disposition des opérationnels, des métriques qu’ils peuvent suivre une
fois l’application déployée en production. Pour cela Spring Boot utilise « Actuator » qui est un
système qui permet de monitorer une application via des URLs spécifiques ou des commandes
disponibles via SSH. Sachez, qu’il est possible de définir vos propres indicateurs très facilement.


## L’architecture ORM

Il apparaît que la programmation objet et les bases de données sont deux mon différent de vue
de leur construction, et qu'il est compliqué de les mettre en relation; c'est la non-
correspondance objet - relationnel. Par exemple, comment représenter l'héritage de classes
dans une base de données? Comment faire pour l'unicité d'attributs d'une relation dans un
schéma de classes, ou bien lorsqu'on est dans le cas d'une clé étrangère?

Or, avec notamment le nombre croissant d'applications smart phones en Java qui ont besoin
d'enregistrer les objets créés, il est intéressant d'un point de vue pratique de pouvoir fabriquer
une base de données à partir de classes pour stocker les objets créés, tout comme inversement
de pouvoir créer des classes hiérarchisées à partir d'une base de données. Il existe une solution
créée par Sun Microsystems il y a plusieurs années et proposée par Oracle, qu’est le JDBC (Java
Database Connectivity), qui permet d'introduire des instructions sen SQL dans un programme
Java, mais le développement est généralement long et laborieux – tout est à faire manuellement
et donc coûteux (30%du coût de création d'un programme). Les recherches se focalisent donc
sur l'idée de construire un outil qui permette aux applications de créer et d'accéder rapidement
et facilement à des données relationnelles d'un point de vue objet, et inversement. C'est ainsi
qu'on est arrivé au object-relational mapping (ORM), dont le principal outil est Hibernate.

L’ORM (Object Relational Mapping), une couche qui effectue la correspondance (mapping) entre
les objets des programmes, et les données tabulaires enregistrées dans la base de données.


## Java JPA

La Java Persistance API (abrégée en JPA), est une interface de programmation Java permettant
aux développeurs d'organiser des données relationnelles dans des applications utilisant la
plateforme Java.

L'utilisation pour la persistance d'un mapping O/R permet de proposer un niveau d'abstraction
plus élevé que la simple utilisation de JDBC : ce mapping permet d'assurer la transformation
d'objets vers la base de données et vice versa que cela soit pour des lectures ou des mises à
jour (création, modification ou suppression). Développée dans le cadre de la version 3.0 des EJB,
cette API ne se limite pas aux EJB puisqu'elle peut aussi être mise en oeuvre dans des
applications Java SE. L'API propose un langage d'interrogation similaire à SQL mais utilisant des
objets plutôt que des entités relationnelles de la base de données.

L'API Java Persistence repose sur des entités qui sont de simples POJOs annotés et sur un
gestionnaire de ces entités (EntityManager) qui propose des fonctionnalités pour les manipuler
(ajout, modification suppression, recherche). Ce gestionnaire est responsable de la gestion de
l'état des entités et de leur persistance dans la base de données.

Donc, Java JPA est un standard de l’ORM basée sur un group des interfaces dans le package
javax.persitance. Ensuit Beaucoup des vendeuses on fait l’implémentation de ces interfaces et
donne des outils ORM ou l’implémentation de l’ORM, comme Red Hat donne le Hibernate et
Oracle donne Toplink. Il y a des outils ORM est commercial comme Kodo.

List des logiciels ou Framework ORM :

-  ActiveJDBC, Java implementation of Active record pattern, inspired by Ruby on Rails
-  Apache Cayenne, open-source for Java
- DataNucleus, open-source JDO and JPA implementation (formerly known as JPOX)
- Ebean, open-source ORM framework
- EclipseLink, Eclipse persistence platform
- Enterprise JavaBeans (EJB)
- Enterprise Objects Framework, Mac OS X/Java, part of Apple WebObjects
- Hibernate, open-source ORM framework, widely used
- Java Data Objects (JDO)
- JOOQ Object Oriented Querying (jOOQ)
- Kodo, commercial implementation of both Java Data Objects and Java Persistence API
- TopLink by Oracle


## Spring boot JPA

Le projet Spring-Data-JPA est l'un des projets de Spring reposant sur Spring-Data. Spring-Data
réduit considérablement le coût économique de la couche d'accès aux données relationnelles
ou NoSQL. Il simplifie l'accès aux bases de données SGBDR ou de type NoSQL.

**Spring-Data** peut-être combiné avec le framework QueryDSL (Domain Driven Design) afin de
réaliser des requêtes complexes.

Avec Spring-Data-JPA, le développeur bénéficie d'un certain nombre de méthodes d'accès à la
base sans écrire une ligne de code d'implémentation.

Le projet Spring-Data-JPA vise à améliorer la mise en œuvre de la couche d'accès aux données
en réduisant considérablement l'effort d'écriture du code d'implémentation en particulier pour
les méthodes CRUD et de recherche.

La notion centrale dans Spring-Data-JPA est la notion "Repository". Le repository est une
interface à écrire par le développeur. Il déclare, dans cette interface, les méthodes utiles d'accès
aux données et Spring-Data-JPA fournissent les implémentations nécessaires.

Avec Spring-Data, écrire des méthodes de requête à la base se fait en 3+1 étapes:

1. Déclarer une interface Repository
2. Ecrire la signature de la méthode de requête (CRUD ou de recherche)
3. Configurer Spring pour la génération automatique
4. Appeler dans le client les méthodes du Repository

Revenir au sujet principal, Spring Data JPA facilite l'implémentation facile de référentiels basés
sur JPA. Ce module traite de la prise en charge améliorée des couches d'accès aux données
basées sur JPA. Il facilite la création d'applications basées sur Spring qui utilisent des
technologies d'accès aux données.

L'implémentation d'une couche d'accès aux données d'une application est fastidieuse depuis
longtemps. Trop de code passe-partout doit être écrit pour exécuter des requêtes simples ainsi
que pour effectuer la pagination et l'audit. Spring Data JPA vise à améliorer considérablement la
mise en œuvre des couches d'accès aux données en réduisant l'effort au montant réellement
nécessaire. En tant que développeur, vous écrivez vos interfaces de référentiel, y compris les
méthodes de recherche personnalisées, et Spring fournira automatiquement l'implémentation.

Caractéristiques :

1. Prise en charge sophistiquée pour créer des référentiels basés sur Spring et JPA
2. Prise en charge des prédicats Querydsl et donc des requêtes JPA sécurisées
3. Audit transparent de la classe de domaine
4. Prise en charge de la pagination, exécution dynamique des requêtes, possibilité
    d'intégrer un code d'accès aux données personnalisée


5. Validation des requêtes annotées @Query au moment du bootstrap
6. Prise en charge du mappage d'entités basé sur XML
7. Configuration du référentiel basée sur JavaConfig en introduisant
    @EnableJpaRepositories


# How to build the project:
1. docker build . --tag demo
2. docker run -it -p9595:9595 demo:latest


